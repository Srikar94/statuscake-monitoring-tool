provider "statuscake" {
        username = "${var.username}"
        apikey = "${var.apikey}"
    }

    resource "statuscake_test" "google" {
        website_name = "www.google.com"
        website_url = "www.google.com"
        test_type = "HTTP"
        check_rate = 300
    }
 
